import React, { Component } from 'react';
import { Navlink, Link } from 'react-router-dom';
import { login, logout, isLoggedIn } from "../utils/AuthService";
import { uploadWidget } from '../utils/WidgetHelper';

import "../App.css";
import Create from "./Create";

class Nav extends Component {
    uploadGif(){
        let cloudinarySettings = {
            cloug_name: 'satrahmadi',
            upload_preset: 'wfwy9sjd',
            tags: ['cliphy'],
            sources: ['local', 'url', 'google_photos', 'facebook'],
            client_allowed_formats: ['gif'],
            keep_widget_open: true,
            theme: 'minimal',
        }
    }


    render() {
        return(
            <nav className="navbar navbar-inverse">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <Link className="navbar-brand" to="/">React Blog App</Link>
                    </div>
                <div>
                    <ul className="nav navbar-nav">
                        <li>
                            <Link to="/">All Post</Link>
                        </li>
                        <li>
                            <Link to="/create">Create Post</Link>
                        </li>
                    </ul>
                    <ul className="nav navbar-nav navbar-right">
                        <li>
                            {
                                (isLoggedIn()) ? <button type="button" className="btn btn-raised btn-sm btn-default" onClick={this.uploadGif}>Upload Gif</button> : ''
                            }
                        </li>
                        <li>
                            {
                                (isLoggedIn()) ? (<button type="button" className="btn btn-raised btn-sm btn-danger" onClick={() => logout()}>Logout</button>) : (<button type="button" className="btn btn-sm btn-raised btn-default" onClick={() => login()}>Login</button>)
                            }
                        </li>
                    </ul>
                </div>
                </div>
            </nav>
        )
    }
}

export default Nav;