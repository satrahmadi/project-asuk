import React, { Component } from 'react';
import { Switch, Route} from 'react-router-dom';
import logo from './logo.svg';
import './App.css';
import './components/Dashboard';
import Dashboard from './components/Dashboard';
import Create from './components/Create';

class App extends Component {
  render() {
    return (
      <div>
          <Route exact path="/" component={Dashboard} />
          <Route path="/create" component={Create} />
      </div>
      
    );
  }
}

export default App;
